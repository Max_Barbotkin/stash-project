import React from "react";

type ContextProps = string[];

const noop = () => null;
const FeatureTogglesContext = React.createContext<Partial<ContextProps>>([]);
const { Consumer, Provider } = FeatureTogglesContext;

export const FeaturesEnabled: React.FC<{
  features: string[];
}> = ({ features = [], children }) => {
  return <Provider value={features}>{children}</Provider>;
};

export const Feature: React.FC<{
  disabled?: React.FC;
  name: string;
}> = ({ disabled: DisabledComponent = noop, name, children }) => (
  <Consumer>
    {(features) => {
      const enabled = features.includes(name);
      return enabled ? children : <DisabledComponent />;
    }}
  </Consumer>
);
export const HasFeatures: React.FC<{
  features: string[];
  disabled?: React.FC;
}> = ({ features = [], disabled: DisabledComponent = noop, children }) => (
  <Consumer>
    {(enabledFeatures) => {
      const enabled = features.some((item) =>
        enabledFeatures.find((enabledItem) => item === enabledItem)
      );
      return enabled ? children : <DisabledComponent />;
    }}
  </Consumer>
);
