import React, { useState, useEffect } from "react";
import { Transition } from "@headlessui/react";

type AlertProps = {
  variant?: "error" | "warning" | "info" | "success";
  show: boolean;
  fullWidth?: boolean;
  delay?: number;
  className?: string;
};

export const Alert: React.FC<AlertProps> = ({
  variant = "info",
  children,
  className = "",
  fullWidth = false,
  show = false,
  delay = 300,
}) => {
  const [open, setOpen] = useState(false);
  useEffect(() => {
    setTimeout(() => {
      setOpen(show);
    }, delay);
  }, [show]);

  return (
    <div className={`z-1000 w-full ${className}`}>
      <Transition
        show={open}
        enter="transition ease-in-out duration-300 transform"
        enterFrom="-translate-y-full"
        enterTo="translate-y-0"
        leave="transition ease-in-out duration-300 transform"
        leaveFrom="translate-y-0"
        leaveTo="-translate-y-full"
      >
        <div
          className={`bg-${variant} w-${
            fullWidth ? "auto" : "max-w-sm md:max-w-lg"
          } flex items-center text-white text-sm font-bold px-4 py-3 relative m-auto `}
          role="alert"
        >
          <svg
            className="w-6 h-6 mr-2"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
            />
          </svg>

          <p className="w-10/12">{children} </p>
          <span className="absolute top-2.5 bottom-0 right-2">
            <button onClick={() => setOpen(false)}>
              <svg
                className="fill-current h-6 w-6 text-white"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
          </span>
        </div>
      </Transition>
    </div>
  );
};
